
from scipy.signal import argrelextrema
from numpy import greater, less, array, arange
from sklearn.cluster import KMeans
import statsmodels.api as sm
import datetime

class PeaksAndValleys(object):
    """A class to deal with peak/valley identification in the context of OandaToCandlestickListCollection objects."""

    #
    # constructor
    #
    def __init__(self, order, collection, price_type, candle_element, trim=True, trim_amount=None):

        #
        # set the trim amount to default if not specified
        #
        if trim_amount == None:
            trim_amount = order

        #
        # keep records
        #
        self.order = order
        self.trim = trim
        self.trim_amount = trim_amount

        #
        # get our source data
        #
        talib_format = collection.cs_list_by_price_type[price_type].get_talib_pattern_recognition_input_format_as_dictionary()
        quote_time_series = talib_format[candle_element]
        self.ts_list = talib_format['timestamp_list']
        self.ts_posix_list = talib_format['timestamp_posix_list']

        #
        # calculate peaks and valleys
        #
        self.peaks_idx = argrelextrema(quote_time_series, greater, order=order)[0]
        self.valleys_idx = argrelextrema(quote_time_series, less, order=order)[0]

        #
        # trim if desired
        #
        n = len(quote_time_series)
        if trim:
            self.peaks_idx = [x for x in self.peaks_idx if x >= trim_amount and x <= n - trim_amount]
            self.valleys_idx = [x for x in self.valleys_idx if x >= trim_amount and x <= n - trim_amount]

        #
        # derive the values and timestamps
        #
        self.peaks_ts = [self.ts_posix_list[i] for i in self.peaks_idx]
        self.peaks_values = [quote_time_series[i] for i in self.peaks_idx]
        self.valleys_ts = [self.ts_posix_list[i] for i in self.valleys_idx]
        self.valleys_values = [quote_time_series[i] for i in self.valleys_idx]

    #
    # cluster
    #
    def cluster_logic(self, indices, timestamps, values, n_clusters, max_iter):

        #
        # apply K-means clustering
        #
        idx_values = array([indices, values]).transpose()
        km = KMeans(n_clusters=n_clusters, max_iter=max_iter)
        km.fit(idx_values)
        self.km_labels = km.labels_

        #
        # get an ORDERED list of the (de-duplicated) km.labels_
        #
        self.km_labels_order = []
        for label in km.labels_:
            if not label in self.km_labels_order:
                self.km_labels_order.append(label)

        #
        # reorganize the data
        #
        cluster_dict = {}
        for i, label in enumerate(km.labels_):
            if not cluster_dict.has_key(label):
                cluster_dict[label] = {'t' : [], 'x' : [], 'y' : []}
            t = timestamps[i]
            x = indices[i]
            y = values[i]
            cluster_dict[label]['t'].append(t)
            cluster_dict[label]['x'].append(x)
            cluster_dict[label]['y'].append(y)

        for label in cluster_dict.keys():
            t = cluster_dict[label]['t']
            ###x = cluster_dict[label]['x']
            y = cluster_dict[label]['y']

            if len(t) > 1:
                T = array([t]).transpose()
                T = sm.add_constant(T)
                y = array([y]).transpose()
                model = sm.OLS(y, T)
                model_results = model.fit()
                slope = model_results.params[-1]

                cluster_dict[label]['model_results'] = model_results
                cluster_dict[label]['slope_normalized'] = slope / abs(model_results.params[0])
                t_predict = array([t[0], t[-1]])
                T_predict = t_predict.reshape(-1, 1)
                T_predict = sm.add_constant(t_predict)
                cluster_dict[label]['t_predict'] = t_predict
                cluster_dict[label]['y_predict'] = model_results.predict(T_predict)
            else:
                cluster_dict[label]['model_results'] = None
                cluster_dict[label]['slope_normalized'] = None
                cluster_dict[label]['t_predict'] = t
                cluster_dict[label]['y_predict'] = y

            cluster_dict[label]['t'] = [datetime.datetime.fromtimestamp(x) for x in cluster_dict[label]['t']]
            cluster_dict[label]['t_predict'] = [datetime.datetime.fromtimestamp(x) for x in cluster_dict[label]['t_predict']]


        return cluster_dict, km.labels_[-1]

    #
    # pilot the clustering
    #
    def cluster(self, n_clusters, max_iter):

        #
        # store configuration
        #
        self.n_clusters = n_clusters
        self.max_iter = max_iter

        #
        # run the clustering logic
        #
        self.peaks_clusters_dict, self.peaks_rightmost_group = self.cluster_logic(self.peaks_idx, self.peaks_ts, self.peaks_values, n_clusters, max_iter)
        self.valleys_clusters_dict, self.valleys_rightmost_group = self.cluster_logic(self.valleys_idx, self.valleys_ts, self.valleys_values, n_clusters, max_iter)


