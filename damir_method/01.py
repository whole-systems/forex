
#
# import useful libraries
#
import json
import sys
import datetime
import pprint as pp
import pandas as pd
import numpy as np
from scipy.signal import argrelextrema
from sklearn.cluster import KMeans
from scipy.stats import pearsonr, spearmanr
import statsmodels.api as sm

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from get_time_series import *
from candle import Candlestick, CandlestickList

#
# time series object
#
class TimeSeriesCollection(object):
    """An object for storing the time series data."""

    def __init__(self, candle_list):
        self.dictionary_form = {'timestamp' : []}
        for candle in candle_list:
            posix_timestamp = float(candle['time'])
            self.dictionary_form['timestamp'].append(posix_timestamp)
            for ask_bid_or_mid in ['ask', 'bid', 'mid']:

                if not self.dictionary_form.has_key(ask_bid_or_mid):
                    self.dictionary_form[ask_bid_or_mid] = {}

                for candle_element in candle[ask_bid_or_mid].keys():
                    if not self.dictionary_form[ask_bid_or_mid].has_key(candle_element):
                        self.dictionary_form[ask_bid_or_mid][candle_element] = []
                    self.dictionary_form[ask_bid_or_mid][candle_element].append(float(candle[ask_bid_or_mid][candle_element]))

        for ask_bid_or_mid in ['ask', 'bid', 'mid']:
            for candle_element in self.dictionary_form[ask_bid_or_mid].keys():
                self.dictionary_form[ask_bid_or_mid][candle_element] = pd.Series(self.dictionary_form[ask_bid_or_mid][candle_element])

    def ema_on_close(self, span, trend_amount):
        self.ema = {}
        for ask_bid_or_mid in ['ask', 'bid', 'mid']:

            self.ema[ask_bid_or_mid] = {'ema' : pd.ewma(self.dictionary_form[ask_bid_or_mid]['c'], span=span)}

            x = range(0, len(self.ema[ask_bid_or_mid]['ema']))[-1 * trend_amount:]
            y = self.ema[ask_bid_or_mid]['ema'].values[-1 * trend_amount:]
            self.ema[ask_bid_or_mid]['pr'] = pearsonr(x, y)
            self.ema[ask_bid_or_mid]['trading_above_ema'] = list(self.dictionary_form[ask_bid_or_mid]['c'])[-1] > list(self.ema[ask_bid_or_mid]['ema'].values)[-1]

            X = np.array([x]).transpose()
            X = sm.add_constant(X)
            y = np.array([y]).transpose()
            model = sm.RLM(y, X, M=sm.robust.norms.HuberT())
            model_results = model.fit()
            slope = model_results.params[-1]
            self.ema[ask_bid_or_mid]['slope_normalized'] = slope / abs(model_results.params[0])


    def plot_close(self, ask_bid_or_mid, filename, title, colors):
        plt.figure()
        plt.plot(self.dictionary_form[ask_bid_or_mid]['c'], color='blue')
        plt.plot(self.ema[ask_bid_or_mid]['ema'], color='magenta')

        #plt.plot(self.peaks[ask_bid_or_mid]['idx'], self.peaks[ask_bid_or_mid]['values'], 'o', color='purple')
        #plt.plot(self.valleys[ask_bid_or_mid]['idx'], self.valleys[ask_bid_or_mid]['values'], 'o', color='black')

        # for group in sorted(self.peaks[ask_bid_or_mid]['kmeans_dict'].keys()):
        #     color = colors[group]
        #     x = self.peaks[ask_bid_or_mid]['kmeans_dict'][group]['x']
        #     y = self.peaks[ask_bid_or_mid]['kmeans_dict'][group]['y']
        #     plt.plot(x, y, 'o', color=color)

        for group in sorted(self.valleys[ask_bid_or_mid]['kmeans_dict'].keys()):
            color = colors[group]
            x = self.valleys[ask_bid_or_mid]['kmeans_dict'][group]['x']
            y = self.valleys[ask_bid_or_mid]['kmeans_dict'][group]['y']
            plt.plot(x, y, 'o', color=color)

        plt.plot(self.peaks[ask_bid_or_mid]['predicted_x'], self.peaks[ask_bid_or_mid]['predicted_y'], color='red')
        plt.plot(self.valleys[ask_bid_or_mid]['predicted_x'], self.valleys[ask_bid_or_mid]['predicted_y'], color='black')


        plt.savefig(filename)
        plt.close()

    def find_peaks_and_valleys_close(self, order, n_clusters):
        self.peaks = {}
        self.valleys = {}
        for ask_bid_or_mid in ['ask', 'bid', 'mid']:
            peaks = argrelextrema(self.dictionary_form[ask_bid_or_mid]['c'].values, np.greater, order=order)[0]
            valleys = argrelextrema(self.dictionary_form[ask_bid_or_mid]['c'].values, np.less, order=order)[0]

            n = len(self.dictionary_form[ask_bid_or_mid]['c'].values)
            peaks = [x for x in peaks if x >= order and x <= n - order]
            valleys = [x for x in valleys if x >= order and x <= n - order]

            peak_values = [self.dictionary_form[ask_bid_or_mid]['c'].values[i] for i in peaks]
            valley_values = [self.dictionary_form[ask_bid_or_mid]['c'].values[i] for i in valleys]
            self.peaks[ask_bid_or_mid] = {'idx' : peaks, 'values' : peak_values}
            self.valleys[ask_bid_or_mid] = {'idx' : valleys, 'values' : valley_values}

            #
            # cluster
            #
            peak_idx_values = np.array([peaks, peak_values]).transpose()
            km = KMeans(n_clusters=n_clusters)
            km.fit(peak_idx_values)
            self.peaks[ask_bid_or_mid]['kmeans'] = km.labels_


            peak_dict = {}
            for i, label in enumerate(km.labels_):
                if not peak_dict.has_key(label):
                    peak_dict[label] = {'x' : [], 'y' : []}

                x = peaks[i]
                y = peak_values[i]

                peak_dict[label]['x'].append(x)
                peak_dict[label]['y'].append(y)

            for label in peak_dict.keys():
                x = peak_dict[label]['x']
                y = peak_dict[label]['y']

                if len(x) != 0:
                    pr = pearsonr(x, y)
                    spr = spearmanr(x, y)
                    peak_dict[label]['pr'] = pr
                    peak_dict[label]['spr'] = spr
                    
                    X = np.array([x]).transpose()
                    X = sm.add_constant(X)
                    y = np.array([y]).transpose()
                    model = sm.OLS(y, X)
                    model_results = model.fit()
                    slope = model_results.params[-1]

                    peak_dict[label]['model_results'] = model_results
                    peak_dict[label]['slope_normalized'] = slope / abs(model_results.params[0])
                    peak_dict[label]['slope'] = slope
                    peak_dict[label]['model_params'] = model_results.params

                else:
                    peak_dict[label]['pr'] = None
                    peak_dict[label]['spr'] = None
                    peak_dict[label]['slope_normalized'] = None

            self.peaks[ask_bid_or_mid]['kmeans_dict'] = peak_dict
            self.peaks[ask_bid_or_mid]['rightmost_group'] = km.labels_[-1]

            x = peak_dict[km.labels_[-1]]['x']
            model_results = peak_dict[km.labels_[-1]]['model_results']
            X_to_predict = range(int(x[0]), n + 1)
            X_to_predict_with_intercept = sm.add_constant(np.array([X_to_predict]).transpose())
            y_predicted = model_results.predict(X_to_predict_with_intercept)
            self.peaks[ask_bid_or_mid]['predicted_x'] = X_to_predict
            self.peaks[ask_bid_or_mid]['predicted_y'] = y_predicted










            valley_idx_values = np.array([valleys, valley_values]).transpose()
            km = KMeans(n_clusters=n_clusters)
            km.fit(valley_idx_values)
            self.valleys[ask_bid_or_mid]['kmeans'] = km.labels_

            valley_dict = {}
            for i, label in enumerate(km.labels_):
                if not valley_dict.has_key(label):
                    valley_dict[label] = {'x' : [], 'y' : []}

                x = valleys[i]
                y = valley_values[i]

                valley_dict[label]['x'].append(x)
                valley_dict[label]['y'].append(y)

            for label in valley_dict.keys():
                x = valley_dict[label]['x']
                y = valley_dict[label]['y']

                if len(x) != 0:
                    pr = pearsonr(x, y)
                    spr = spearmanr(x, y)
                    valley_dict[label]['pr'] = pr
                    valley_dict[label]['spr'] = spr
                    
                    X = np.array([x]).transpose()
                    X = sm.add_constant(X)
                    y = np.array([y]).transpose()
                    model = sm.OLS(y, X)
                    model_results = model.fit()
                    slope = model_results.params[-1]

                    valley_dict[label]['model_results'] = model_results
                    valley_dict[label]['slope'] = slope
                    valley_dict[label]['slope_normalized'] = slope / abs(model_results.params[0])
                    valley_dict[label]['model_params'] = model_results.params
                else:
                    valley_dict[label]['pr'] = None
                    valley_dict[label]['spr'] = None
                    valley_dict[label]['slope_normalized'] = None


            self.valleys[ask_bid_or_mid]['kmeans_dict'] = valley_dict
            self.valleys[ask_bid_or_mid]['rightmost_group'] = km.labels_[-1]

            x = valley_dict[km.labels_[-1]]['x']
            model_results = valley_dict[km.labels_[-1]]['model_results']
            X_to_predict = range(int(x[0]), n + 1)
            X_to_predict_with_intercept = sm.add_constant(np.array([X_to_predict]).transpose())
            y_predicted = model_results.predict(X_to_predict_with_intercept)
            self.valleys[ask_bid_or_mid]['predicted_x'] = X_to_predict
            self.valleys[ask_bid_or_mid]['predicted_y'] = y_predicted



    
    def last_candlesticks(self, recent_price_window, candlestick_arguments):
        n = len(list(self.ema['ask']['ema'].values))

        self.candlesticks = {}
        for i in range(0, n):

            for ask_bid_or_mid in ['ask', 'bid', 'mid']:

                if not self.candlesticks.has_key(ask_bid_or_mid):
                    self.candlesticks[ask_bid_or_mid] = []

                ts = self.dictionary_form['timestamp'][i]
                O = list(self.dictionary_form[ask_bid_or_mid]['o'].values)[i]
                C = list(self.dictionary_form[ask_bid_or_mid]['c'].values)[i]
                L = list(self.dictionary_form[ask_bid_or_mid]['l'].values)[i]
                H = list(self.dictionary_form[ask_bid_or_mid]['h'].values)[i]

                self.candlesticks[ask_bid_or_mid].append(Candlestick(ts, O, H, L, C))

        self.last_candlesticks_list = {
            'ask' : self.candlesticks['ask'][-1 * recent_price_window:],
            'bid' : self.candlesticks['bid'][-1 * recent_price_window:],
            'mid' : self.candlesticks['mid'][-1 * recent_price_window:],
        }

    def should_we_make_a_trade(self, ask_bid_or_mid, ema_slope_cutoff, support_resistance_slope_cutoff, ema_hit_percent_diff_cutoff, recent_price_window):

        #############
        #    EMA    #
        #############

        # does EMA have slope?
        ema_slope_norm = self.ema[ask_bid_or_mid]['slope_normalized']
        if abs(ema_slope_norm) < ema_slope_cutoff:  return False
        sign_ema_slope_norm = int(round(np.sign(ema_slope_norm)))

        # is trading on same side as the EMA trend?
        trading_above = int(self.ema[ask_bid_or_mid]['trading_above_ema'])
        if trading_above == 0:
            trading_above = -1
        print 'trading above: ', trading_above, sign_ema_slope_norm
        if trading_above != sign_ema_slope_norm:  return False



        ############################
        #    support/resistance    #
        ############################

        print sign_ema_slope_norm
        if sign_ema_slope_norm == 1:
            sup_res = self.peaks[ask_bid_or_mid]
        elif sign_ema_slope_norm == -1:
            sup_res = self.valleys[ask_bid_or_mid]
        else:
            return False

        # ensure rightmost is not None
        rightmost_group = sup_res['rightmost_group']
        if sup_res['kmeans_dict'][rightmost_group]['slope_normalized'] == None:
            return False

        # check that the end support/resistance group runs opposite to the EMA trend
        slope_normalized = sup_res['kmeans_dict'][rightmost_group]['slope_normalized']
        model_params = sup_res['kmeans_dict'][rightmost_group]['model_params']
        slope = sup_res['kmeans_dict'][rightmost_group]['slope']
        if abs(slope_normalized) < support_resistance_slope_cutoff:
            return False
        sign_slope_normalized = int(round(np.sign(slope_normalized)))
        print sign_slope_normalized, sign_ema_slope_norm, slope_normalized, slope, model_params

        if sign_slope_normalized == sign_ema_slope_norm:
            print 'sign_slope_normalized == sign_ema_slope_norm'
            return False


        ###################
        #    EMA again    #
        ###################

        # have we hit the EMA with a recent price?
        n = len(self.last_candlesticks_list[ask_bid_or_mid])
        found_hit = False
        for i in range(n - 1, 0, -1):
            idx = i - 1
            percent_diff = 100. * (self.last_candlesticks_list[ask_bid_or_mid][idx].close - list(self.ema[ask_bid_or_mid]['ema'].values)[idx]) / self.last_candlesticks_list[ask_bid_or_mid][idx].close
            if abs(percent_diff) < ema_hit_percent_diff_cutoff:
                found_hit = True
                break
        if not found_hit:
            return False




        ##############################
        #    candlestick analysis    #
        ##############################

        candle_list = self.last_candlesticks_list[ask_bid_or_mid]
        print 'Idx: ', idx
        for i in range(len(candle_list) - 1, idx, -1):
            candle = candle_list[i]
            print candle.bull_bear_or_neither, candle.open, candle.close, candle.low, candle.high
            




        return True



#
# main
#
if __name__ == '__main__':

    #
    # user settings
    #
    config_file = sys.argv[1]
    order = 5
    n_clusters = 7
    span = 200
    ema_trend_amount = 200
    trade_computation_price = 'mid'
    ema_slope_cutoff = 0.0001
    support_resistance_slope_cutoff = 0.0001
    ema_hit_percent_diff_cutoff = 10.
    recent_price_window = 150

    colors = {0: 'orange', 1 : 'blue', 2 : 'yellow', 3 : 'magenta', 4 : 'red', 5 : 'purple', 6 : 'grey'}


    #
    # load configuration
    #
    with open(config_file) as f:
        config = json.load(f)

    #
    # save candles for training set
    #
    str_time = str(datetime.datetime.now()).replace(' ', '-')
    candles = load_historical_data(config, config['training_n'])

    #
    # TEMP
    #
    print
    pp.pprint(candles)
    print

    #
    # generate time series objects
    #
    instrument_dict = {}
    for instrument in candles.keys():
        instrument_dict[instrument] = TimeSeriesCollection(candles[instrument]['candles'])

    #
    # compute 200-EMA
    #
    for instrument in candles.keys():
        instrument_dict[instrument].ema_on_close(float(span), ema_trend_amount)

    #
    # compute peaks and valleys
    #
    for instrument in candles.keys():
        instrument_dict[instrument].find_peaks_and_valleys_close(order, n_clusters)

    #
    # compute last candlestick info
    #
    for instrument in candles.keys():
        instrument_dict[instrument].last_candlesticks(recent_price_window, candlestick_arguments)

    #
    # should we make a trade?
    #
    for instrument in candles.keys():
        print
        print instrument
        trade = instrument_dict[instrument].should_we_make_a_trade(trade_computation_price, ema_slope_cutoff, support_resistance_slope_cutoff, ema_hit_percent_diff_cutoff, recent_price_window)
        print trade



    #
    # plot something
    #
    for instrument in candles.keys():
        instrument_dict[instrument].plot_close('ask', config['plot_directory'] + '/' + instrument + '.png', instrument, colors)


    #
    # plot a candlestick plot
    #
    my_list = instrument_dict['USD_CAD'].last_candlesticks_list['mid']
    cs_list_stats = CandlestickList(my_list)
    cs_list_stats.plot('USD/CAD', config['plot_directory'] + '/' + 'USD_CAD' + '_cs.png')
