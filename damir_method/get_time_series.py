#
# import useful libraries
#
import requests
import pandas as pd



#
# get historical data from Oanda
#
def load_historical_data(config, N):

    #
    # define variables
    #
    base_url = 'https://' + config['hostname'] + '/v3'

    #
    # define header
    #
    headers = {
        'Authorization' : 'Bearer ' + config['token'],
        'Accept-Datetime-Format' : 'UNIX',
        }

    #
    # get candles for instruments
    #
    candles = {}
    payload = {
        'count' : N,
        'granularity' : config['granularity'],
        'price' : 'BAM',
        }
    for instr in config['instruments']:
        r = requests.get(base_url + '/instruments/' + instr + '/candles', params=payload, headers=headers)
        candles[instr] = r.json()

    return candles

