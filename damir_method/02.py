
#
# import useful libraries
#
import sys
import json
import pprint as pp
import talib

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from get_time_series import *
from forex.candle import *
from forex.trend import *
from forex.peaks_and_valleys import *

#
# main
#
if __name__ == '__main__':

    #
    # user settings
    #
    download_candles = False
    plot_stuff = True
    config_file = sys.argv[1]
    ema_timespan = 200
    peak_valley_order = 5
    n_clusters = 7
    max_iter = 5000
    price_type = 'mid'

    colors = {0: 'orange', 1 : 'blue', 2 : 'yellow', 3 : 'magenta', 4 : 'red', 5 : 'purple', 6 : 'grey'}

    #
    # load configuration
    #
    with open(config_file) as f:
        config = json.load(f)

    #
    # download candles
    #
    if download_candles:
        candles = load_historical_data(config, config['training_n'])
    
        #
        # save candles
        #
        with open(config['output_directory'] + '/candles.json', 'w') as f:
            json.dump(candles, f)
    else:
        with open(config['output_directory'] + '/candles.json') as f:
            candles = json.load(f)

    #
    # define collections for each instrument
    #
    instruments = {}
    for instr in candles.keys():
        collection = OandaToCandlestickListCollection(candles[instr]['candles'])
        instruments[instr] = collection

    #
    # compute EMA
    #
    EMA = {}
    for instr in instruments.keys():
        EMA[instr] = Trend()
        EMA[instr].calculate_EMA(ema_timespan, instruments[instr], price_type, 'close')

    #
    # compute peaks and valleys
    #
    PV = {}
    for instr in instruments.keys():
        PV[instr] = PeaksAndValleys(peak_valley_order, instruments[instr], price_type, 'close')

    #
    # cluster
    #
    for instr in instruments.keys():
        PV[instr].cluster(n_clusters, max_iter)

    #
    # get quotes for plotting
    #
    quotes = {}
    for instr in instruments.keys():
        quotes[instr] = instruments[instr].cs_list_by_price_type[price_type].get_talib_pattern_recognition_input_format_as_dictionary()
        
    #
    # plot
    #
    if plot_stuff:
        for instr in instruments.keys():
            plt.figure(figsize=[12, 10])
            plt.plot(quotes[instr]['timestamp_list'], quotes[instr]['close'], color='blue', label='Time Series')
            plt.plot(EMA[instr].ema_ts_truncated, EMA[instr].ema_price_truncated, color='magenta', label='EMA-200')
            #plt.plot(PV[instr].peaks_ts, PV[instr].peaks_values, 'o', color='cyan', label='Detected Peaks')
            #plt.plot(PV[instr].valleys_ts, PV[instr].valleys_values, 'o', color='green', label='Detected Valleys')

            # # clusters
            # for i, label in enumerate(sorted(PV[instr].peaks_clusters_dict.keys())):
            #     t = PV[instr].peaks_clusters_dict[label]['t']
            #     y = PV[instr].peaks_clusters_dict[label]['y']
            #     color = colors[i]
            #     plt.plot(t, y, 'o', color=color)
            # for i, label in enumerate(sorted(PV[instr].valleys_clusters_dict.keys())):
            #     t = PV[instr].valleys_clusters_dict[label]['t']
            #     y = PV[instr].valleys_clusters_dict[label]['y']
            #     color = colors[len(colors.keys()) - 1 - i]
            #     plt.plot(t, y, 'o', color=color)

            # cluster regression lines
            for i, label in enumerate(sorted(PV[instr].peaks_clusters_dict.keys())):
                t = PV[instr].peaks_clusters_dict[label]['t_predict']
                y = PV[instr].peaks_clusters_dict[label]['y_predict']
                plt.plot(t, y, '-', color='orange')
            for i, label in enumerate(sorted(PV[instr].valleys_clusters_dict.keys())):
                t = PV[instr].valleys_clusters_dict[label]['t_predict']
                y = PV[instr].valleys_clusters_dict[label]['y_predict']
                plt.plot(t, y, '-', color='red')



            plt.legend()
            plt.title(instr.replace('_', '/'), fontsize=18)
            plt.xlabel('Date')
            plt.ylabel('Closing Asking Price')
            plt.savefig(config['plot_directory'] + '/' + instr + '.png')
            plt.close()
