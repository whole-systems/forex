
#
# useful libraries
#
import os
import pickle
import pandas as pd
import pprint as pp
import numpy as np
import os
import datetime

#
# user settings
#
input_files = ['output/stream_results.2017-09-20_00:03:07.592594.pickle.gz', 'output/stream_results.2017-09-20_00:35:39.945861.pickle.gz', 'output/stream_results.2017-09-20_01:06:38.515654.pickle.gz']

instrument_to_mess_with = 'USD_CAD'
use = 'asks'
granularity = 1

#
# iterate through the input files
#
full_df = None
have_full_df = False
for filename in input_files:

    #
    # unzip and load data
    #
    os.system('gunzip ' + filename)
    with open(filename.replace('.gz', '')) as f:
        quotes = pickle.load(f)

    #
    # average the bids and asks (REVIEW THIS IDEA)
    #
    for entry in quotes:
        entry['asks'] = np.mean(entry['asks'])
        entry['bids'] = np.mean(entry['bids'])

    #
    # Initialize or add to data frame
    #
    df = pd.DataFrame(quotes)
    if not have_full_df:
        full_df = df
        have_full_df = True
    else:
        full_df.append(df)

    #
    # compress data file
    #
    os.system('gzip ' + filename.replace('.gz', ''))

#
# sort by time
#
sorted_df = full_df.sort_values('time')

#
# extract the currency we are exploring
#
mess_df = sorted_df.ix[sorted_df['instrument'] == instrument_to_mess_with,]

#
# deal with dates
#
start_time = list(mess_df['time'])[0]
end_time = list(mess_df['time'])[-1]
seconds_to_use = (end_time - start_time).seconds
results_values_list = []
results_ts_list = []
for current_moment in np.arange(0, int(round(float(seconds_to_use) / float(granularity)))):
    value_list = []
    for value, ts in zip(mess_df[use], mess_df['time']):
        if (start_time + datetime.timedelta(seconds = current_moment * granularity)) <= ts < (start_time + datetime.timedelta(seconds = current_moment * granularity) + datetime.timedelta(seconds=granularity)):
            value_list.append(value)
        
        if len(value_list) == 0:
            value_list = [results_values_list[-1]]

        results_values_list.append(np.mean(value_list))
        results_ts_list.append(current_moment)    
                          
#
# fft
#
results_values_list = np.array(results_values_list)
fft_results = np.fft.fft(results_values_list)

print
pp.pprint(fft_results)
