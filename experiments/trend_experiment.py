
#
# useful libraries
#
import os
import pickle
import pandas as pd
import pprint as pp
import numpy as np
import scipy.stats as stats
import os
import datetime

#
# user settings
#
input_files = ['../output/stream_results.2017-09-20_00:03:07.592594.pickle.gz', '../output/stream_results.2017-09-20_00:35:39.945861.pickle.gz', '../output/stream_results.2017-09-20_01:06:38.515654.pickle.gz']

instrument_to_mess_with = 'USD_CAD'
use = 'asks'
N = 5
look_ahead = 5
p_cutoff = 0.05

#
# iterate through the input files
#
full_df = None
have_full_df = False
for filename in input_files:

    #
    # unzip and load data
    #
    os.system('gunzip ' + filename)
    with open(filename.replace('.gz', '')) as f:
        quotes = pickle.load(f)

    #
    # average the bids and asks (REVIEW THIS IDEA)
    #
    for entry in quotes:
        entry['asks'] = np.mean(entry['asks'])
        entry['bids'] = np.mean(entry['bids'])

    #
    # Initialize or add to data frame
    #
    df = pd.DataFrame(quotes)
    if not have_full_df:
        full_df = df
        have_full_df = True
    else:
        full_df.append(df)

    #
    # compress data file
    #
    os.system('gzip ' + filename.replace('.gz', ''))

#
# sort by time
#
sorted_df = full_df.sort_values('time')

#
# extract the currency we are exploring
#
mess_df = sorted_df.ix[sorted_df['instrument'] == instrument_to_mess_with,]

#
# iterate using last N entries
#
target_list = []
y_list = []
time_diff_list = []

for i in range(len(mess_df.index) - N - look_ahead):
    values_list = mess_df[use][i:(i+N)]
    ts_list = mess_df['time'][i:(i+N)]
    y = list(mess_df[use])[i + N + look_ahead]
    ts_y = list(mess_df['time'])[i + N + look_ahead]

    r, p = stats.pearsonr(values_list, [float(x.strftime('%s')) for x in ts_list])
    if p <= p_cutoff:
        start = list(values_list)[0]
        end = list(values_list)[-1]
        diff = end - start
        target = end + r * look_ahead * abs(diff)
        
        y_list.append(y)
        target_list.append(target)
        time_diff_list.append( (ts_y - list(ts_list)[-1]).seconds)

print
print stats.spearmanr(y_list, target_list)
print len(target_list), len(mess_df.index)
print np.mean(time_diff_list), np.median(time_diff_list)
print
