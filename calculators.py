


#
# shift and multiplier
#
def get_shift_and_multiplier(instrument):
    """Accounts for the weirdness of the JPY pairs. Future versions might account for other weird pairs. I don't like the use of hard-coded values here. Maybe something dynamic is more appropriate."""

    is_yen = instrument.upper().find('JPY') >= 0

    shift = 4
    if is_yen:
        shift = 2
        
    multiplier = 0.0001
    if is_yen:
        multiplier = 0.01

    return shift, multiplier

#
# number in pips
#
def exchange_rate_in_pips(x, instrument):
    """Represents a currency pair's price in pips."""

    x_list = str(x).split('.')

    shift, multiplier = get_shift_and_multiplier(instrument)

    decimal_side = x_list[1]

    rhs = decimal_side[0:shift]
    if rhs == '':  rhs = '0'
    lhs = decimal_side[shift:]
    if lhs == '':  lhs = '0'

    if x_list[0] == '0':
        to_return = float(rhs + '.' + lhs)
    else:
        to_return = float(x_list[0] + rhs + '.' + lhs)

    return to_return

#
# pip difference
#
def pip_difference(x, y, instrument):
    """Returns y - x in pips, where y and x are exchange rates."""
    x_in_pips = exchange_rate_in_pips(x, instrument)
    y_in_pips = exchange_rate_in_pips(y, instrument)
    return y_in_pips - x_in_pips

#
# spread
#
def spread(bid, ask, instrument):
    """Convenient wrapper for pip_difference(bid, ask, instrument)."""
    return pip_difference(bid, ask, instrument)

#
# determine pip value
#
def determine_pip_change_value_in_base_currency(trade_amount_in_base_currency, pip_change, exchange_rate_at_close, instrument):

    """Determines the value (in the base currency) of a pip change given the exchange rate at position close."""

    shift, multiplier = get_shift_and_multiplier(instrument)
    number_of_quote_currency_per_pip = trade_amount_in_base_currency * multiplier
    base_value_per_pip = number_of_quote_currency_per_pip / exchange_rate_at_close
    value_change_in_base_currency = pip_change * base_value_per_pip
    return(value_change_in_base_currency)


#
# example usage
#
def examples():

    print
    instrument = 'USD/CAD'
    bid = 0.78554
    ask = 0.78563
    print spread(bid, ask, instrument)

    print
    instrument = 'EUR/JPY'
    bid = 88.641
    ask = 88.660
    print spread(bid, ask, instrument)

    print
    print determine_pip_change_value_in_base_currency(350000, 29, 0.8714, 'EUR/GBP')
    print determine_pip_change_value_in_base_currency(175000, -17, 1.2703, 'AUD/NZD')
    print determine_pip_change_value_in_base_currency(500000, 18, 83.84, 'CHF/JPY')
    print determine_pip_change_value_in_base_currency(200000, -27, 91.16, 'USD/JPY')
    print


#
# convert value from one currency to another
#
def convert_value_from_one_currency_to_another(value_in_currency_you_hold, currency_you_hold, currency_you_want, exchange_rate):
    """To be implemented."""
    pass

#
# convert value from one currency to USD
#
def convert_value_from_one_currency_to_USD(value_in_currency_you_hold, currency_you_hold, exchange_rate):
    """Convenient wrapper for 'convert_value_from_one_currency_to_another'(..')."""

    if currency_you_hold == 'USD':
        return value
    else:
        return convert_value_from_one_currency_to_another(value_in_currency_you_hold, currency_you_hold, 'USD', exchange_rate)


#
# run the examples
#
if __name__ == '__main__':
    examples()
