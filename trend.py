
#
# import useful libraries
#
import talib


class Trend(object):
    """A class to deal with trend calculations in the context of OandaToCandlestickListCollection objects."""

    #
    # exponential moving average
    #
    def calculate_EMA(self, timeperiod, collection, price_type, candle_element):
        """e.g.:  EMA(200, collection, 'ask', 'close')"""

        #
        # get our source data
        #
        talib_format = collection.cs_list_by_price_type[price_type].get_talib_pattern_recognition_input_format_as_dictionary()
        quote_time_series = talib_format[candle_element]
        ts_list = talib_format['timestamp_list']

        #
        # keep a record of the time period
        #
        self.time_period = timeperiod

        #
        # run EMA
        #
        self.ema = talib.EMA(quote_time_series, timeperiod)

        #
        # get a copy of the EMA output without NAN, along with and the associated timestamps
        #
        self.ema_price_truncated = self.ema[(timeperiod - 1):]
        self.ema_ts_truncated = ts_list[(timeperiod - 1):]
