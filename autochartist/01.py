
#
# import useful libraries
#
import sys
import json
import requests
import pprint as pp
import time

import forex.calculators as cal

#
# main
#
if __name__ == '__main__':

    #
    # user settings
    #
    config_file = sys.argv[1]
    API_request_interval = 30.
    stop_loss_percentage = 100. / 3.

    #
    # load configuration
    #
    with open(config_file) as f:
        config = json.load(f)

    #
    # define variables
    #
    base_labs_url = 'https://' + config['hostname'] + '/labs/v1/signal/autochartist'

    #
    # define header
    #
    headers = {
        'Authorization' : 'Bearer ' + config['token'],
        'Accept-Datetime-Format' : 'UNIX',
        }


    #
    # prepare for loop
    #
    traded_upon = {}

    #
    # loop
    #
    while True:

        #
        # wait a bit between API requests
        #
        time.sleep(API_request_interval)

        #
        # get patterns
        #
        payload = {}
        r = requests.get(base_labs_url, params=payload, headers=headers)
        result = r.json()

        #
        # iterate through the signals
        #
        traded_upon_this_iteration = {}
        for signal in result['signals']:

            #
            # extract some features
            #
            autochartist_id = signal['id']
            is_completed = signal['meta']['completed'] == 1
            instrument = signal['instrument']
            autochartist_type = signal['type']

            #
            # see if we've seen this one before
            #
            if traded_upon.has_key(autochartist_id):
                continue

            #
            # only use major instruments for now
            #
            if not instrument in config['instruments']:
                continue

            # #
            # # display information for further analysis
            # #
            # print
            # print time.time()
            # if is_completed:
            #     print 'Complete'
            # else:
            #     print 'Inomplete'
            # print
            # pp.pprint(signal)



            #
            # has a pattern completed?
            #
            if is_completed:
                
                #
                # triage by autochartist type
                #
                if autochartist_type == 'keylevel':
                    # trade!
                    # do something with this later
                    pass   # for now

                elif autochartist_type == 'chartpattern':
                    # trade
                    prediction = signal['data']['prediction']
                    price_high = prediction['pricehigh']
                    price_low = prediction['pricelow']
                    time_from = prediction['timefrom']
                    time_to = prediction['timeto']

                    direction = signal['meta']['direction']

                    pip_difference_low_high = cal.pip_difference(price_low, price_high, instrument)
                    take_profit = int(round(pip_difference_low_high))
                    stop_loss = int(round(pip_difference_low_high * stop_loss_percentage / 100.))
                    print instrument, direction, take_profit, stop_loss

                    traded_upon_this_iteration[autochartist_id] = None

        #
        # update record of trades
        #
        traded_upon = traded_upon_this_iteration
