
#
# import useful libraries
#
import requests
import json
import sys
import pprint as pp
import datetime
import numpy as np
import os
import pickle

def retrieve_and_save_stream():
    """Main function for stream.py"""

    #
    # user settings
    #
    config_file = sys.argv[1]
    output_file = sys.argv[2]
    chunk_size = 10000

    #
    # load configuration
    #
    with open(config_file) as f:
        config = json.load(f)

    #
    # define variables
    #
    base_url = 'https://' + config['stream_hostname'] + '/v3/accounts/' + config['account'] + '/pricing/stream?instruments='

    #
    # add instruments
    #
    instruments = '%2C'.join(config['instruments'])
    url = base_url + instruments

    #
    # define header
    #
    headers = {
        'Authorization' : 'Bearer ' + config['token'],
        'Accept-Datetime-Format' : 'UNIX',
        }

    #
    # initiate reading of stream
    #
    r = requests.get(url, headers=headers, stream=True)

    #
    # read stream
    #
    results = []
    count = 0
    for line in r.iter_lines():
        json_line = json.loads(line)

        if json_line['type'] == 'HEARTBEAT':  continue

        del(json_line['type'])
        del(json_line['tradeable'])
        del(json_line['status'])

        json_line['closeoutAsk'] = float(json_line['closeoutAsk'])
        json_line['closeoutBid'] = float(json_line['closeoutBid'])
            
        json_line['asks'] = np.array([float(x['price']) for x in json_line['asks']])
        json_line['bids'] = np.array([float(x['price']) for x in json_line['bids']])

        json_line['time'] = datetime.datetime.fromtimestamp(float(json_line['time']))

        results.append(json_line)

        count += 1
        if count > chunk_size:
            ts = str(datetime.datetime.now()).replace(' ', '_')
            with open(output_file + '.' + ts + '.pickle', 'w') as f:
                pickle.dump(results, f)
            os.system('nohup gzip ' + output_file + '.' + ts + '.pickle &')
            count = 0
            results = []




    f.close()



if __name__ == '__main__':
    retrieve_and_save_stream()
