#
# import useful libraries
#
from numpy import percentile, array
import datetime

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from matplotlib.finance import candlestick_ohlc
from matplotlib.dates import date2num


class Candlestick(object):
    """A class for handling candlestick information."""

    #
    # constructor
    #
    def __init__(self, posix_time, O, H, L, C):
        self.posix_time = posix_time
        self.datetime = datetime.datetime.fromtimestamp(posix_time)
        self.open = O
        self.close = C
        self.low = L
        self.high = H
        self.determine_bull_or_bear()

    #
    # determine whether the candlestick is bullish or bearish
    #
    def determine_bull_or_bear(self):
        if self.close - self.open > 0.:
            self.bull_bear_or_neither = 'bull'
        elif self.close - self.open == 0.:
            self.bull_bear_or_neither = 'neither'
        elif self.close - self.open < 0:
            self.bull_bear_or_neither = 'bear'
        


class CandlestickList(object):
    """Code to deal with lists of Candlesticks."""

    #
    # constructor
    #
    def __init__(self, cslist):
        self.list = cslist

    #
    # converts the list into the format required by TA-Lib's pattern recognition functions, returns as tuple
    #
    def get_talib_pattern_recognition_input_format(self):
        ts_list = []
        ts_posix_list = []
        O_list = []
        C_list = []
        L_list = []
        H_list = []
        for cs in self.list:
            ts_list.append(cs.datetime)
            ts_posix_list.append(cs.posix_time)
            O_list.append(cs.open)
            C_list.append(cs.close)
            L_list.append(cs.low)
            H_list.append(cs.high)
        return ts_list, ts_posix_list, array(O_list), array(H_list), array(L_list), array(C_list)

    #
    # converts the list into the format required by TA-Lib's pattern recognition functions, returns as dictionary
    #
    def get_talib_pattern_recognition_input_format_as_dictionary(self):
        ts_list, ts_posix_list, O_list, H_list, L_list, C_list = self.get_talib_pattern_recognition_input_format()
        return {
            'timestamp_list' : ts_list,
            'timestamp_posix_list' : ts_posix_list,
            'open' : O_list,
            'high' : H_list,
            'low' : L_list,
            'close' : C_list,
        }

    #
    # plot using Matplotlib's finance tools
    #
    def plot(self, title, filename):
        sequence = []
        for cs in self.list:
            sequence.append( (date2num(cs.datetime), cs.open, cs.close, cs.high, cs.low) )
        
        fig, ax = plt.subplots()
        candlestick_ohlc(ax, sequence, width=0.1)
        plt.title(title)
        plt.savefig(filename)
        plt.close()


class OandaToCandlestickListCollection(object):
    """Converts the list found in {instrument : {'candles' : [...]}} to the objects defined by this module."""
    
    #
    # constructor
    #
    def __init__(self, cs_list_from_oanda):

        #
        # create lists of Candlestick objects for each price type (ask, mid, or bid)
        #
        self.cs_list_by_price_type = {'ask' : [], 'mid' : [], 'bid' : []}
        for entry in cs_list_from_oanda:
            ts = float(entry['time'])
            for price_type in ['ask', 'mid', 'bid']:
                cs = Candlestick(ts, float(entry[price_type]['o']), float(entry[price_type]['h']), float(entry[price_type]['l']), float(entry[price_type]['c']))
                self.cs_list_by_price_type[price_type].append(cs)

        #
        # convert each list to a CandlestickList object
        #
        for price_type in self.cs_list_by_price_type.keys():
            self.cs_list_by_price_type[price_type] = CandlestickList(self.cs_list_by_price_type[price_type])
